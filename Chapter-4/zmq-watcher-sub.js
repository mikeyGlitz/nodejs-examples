"use strict";
const
    zmq = require("zmq"),
    
    //Create the Subscriber endpoint
    subscriber = zmq.socket('sub');

//subscribe to all messages
subscriber.subscribe("");

//Handle messages from publisher
subscriber.on("message", function(data){
    let
        message = JSON.parse(data),
        date = new Date(message.timestamp);
        console.log("File: '" + message.file + "' changed at " + date);
});

//Connect to the publisher
subscriber.connect("tcp://localhost:5432");