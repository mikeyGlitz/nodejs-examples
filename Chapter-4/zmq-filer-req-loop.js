"use strict";
const
    zmq = require("zmq"),
    filename = process.argv[2],
    //Create request endpoint
    requester = zmq.socket('req');
    
//handle replies from the responder
requester.on("message", function(data){
    let response = JSON.parse(data);
    
    console.log("Recceived response:", response);
});

requester.connect("tcp://localhost:5433");

//send request for content
for(let i = 1; i <=3; i++){
    console.log("Sending request" + i + " for " + filename);
    requester.send(JSON.stringify({
        path : filename
    }));
}