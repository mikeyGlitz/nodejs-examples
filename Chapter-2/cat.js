#!/usr/bin/env node -- harmony

/*
 * This is a Node program that emulates what the cat binary does in linux systems
 */

require("fs").createReadStream(process.argv[2]).pipe(process.stdout);