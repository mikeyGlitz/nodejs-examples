const fs = require("fs"),
/*
 * process.argv is an array containing the command-line arguments as vectors
 * the first two elements of process.argv are the node command and the name
 * of the javascript file being run. Additionally-supplied arguments are
 * stored in process.argv indexes 2 and above.
 */
    filename = process.argv[2];
    
//This program will throw an error if there is no file name specified
if(!filename){
    throw Error("A file to watch must be specified!")
}

fs.watch(filename, function(){
    console.log("File " + filename + " just changed!");
});

console.log("Now watching " + filename + " for changes...");